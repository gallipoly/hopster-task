//
//  BasicLayer.cpp
//

#include "BasicLayer.h"
#include "Constants.h"


BasicLayer::BasicLayer(shared_ptr<AssetsLoader> builder)
: _builder(builder)
{
    setonEnterTransitionDidFinishCallback([this](){
        auto listener = EventListenerTouchOneByOne::create();
        listener->onTouchBegan = CC_CALLBACK_2(BasicLayer::onTouchBegan, this);
        listener->onTouchMoved = CC_CALLBACK_2(BasicLayer::onTouchMoved, this);
        listener->onTouchEnded = CC_CALLBACK_2(BasicLayer::onTouchEnded, this);
        listener->onTouchCancelled = CC_CALLBACK_2(BasicLayer::onTouchCancelled, this);
        _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
        listener->setEnabled( _lockTouchesCounter == 0 );
        _touchListener = listener;
        
        _eventListeners.push_back(_eventDispatcher->addCustomEventListener(USER_INTERACTION_WILL_LOCK_EVENT, [this](EventCustom* event) {
            lockTouches();
        }));
        _eventListeners.push_back(_eventDispatcher->addCustomEventListener(USER_INTERACTION_WILL_UNLOCK_EVENT, [this](EventCustom* event) {
            unlockTouches();
        }));
    });
    
    _winSize = Director::getInstance()->getWinSize();
}

BasicLayer::~BasicLayer() {
    for (auto listener : _eventListeners)
        _eventDispatcher->removeEventListener(listener);
}

void BasicLayer::lockTouches() {
    ++_lockTouchesCounter;
    if (_lockTouchesCounter == 1) {
        if (_touchListener)
            _touchListener->setEnabled(true);
        onTouchesLocked();
    }
}

void BasicLayer::unlockTouches() {
    --_lockTouchesCounter;
    CCASSERT(_lockTouchesCounter >= 0, "ERROR: wrong usage of touches locker!");
    if (_lockTouchesCounter == 0) {
        if (_touchListener)
            _touchListener->setEnabled(true);
        onTouchesUnlocked();
    }
}
