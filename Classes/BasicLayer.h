//
//  BasicLayer.h
//

#ifndef __BasicLayer__
#define __BasicLayer__

#include "AssetsLoader.h"

using namespace c2d;

class BasicLayer : public cocos2d::Layer {
public:
    static BasicLayer* create(shared_ptr<AssetsLoader> builder);
    
    BasicLayer(shared_ptr<AssetsLoader> builder);
    virtual ~BasicLayer();
    
    virtual auto isTouchesLocked()->bool    { return _lockTouchesCounter != 0; }
    
    virtual auto lockTouches()->void;
    virtual auto unlockTouches()->void;
    
    virtual auto onTouchesLocked()->void    { /* override me */ }
    virtual auto onTouchesUnlocked()->void  { /* override me */ }
    
    virtual auto onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)->bool override        { /* override me */ return false; }
    virtual auto onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event)->void override        { /* override me */ }
    virtual auto onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event)->void override        { /* override me */ }
    virtual auto onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* event)->void override    { /* override me */ }

protected:
    shared_ptr<AssetsLoader>        _builder            = nullptr;
    
    cocos2d::EventListener*         _touchListener      = nullptr;
    vector<cocos2d::EventListener*> _eventListeners;
    
    int                             _lockTouchesCounter = 0;
    
    Size                            _winSize;
};

#endif
