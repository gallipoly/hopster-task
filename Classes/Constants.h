//
//  Constants.h
//

#ifndef Constants_h
#define Constants_h

// events
const string USER_INTERACTION_WILL_LOCK_EVENT   = "USER_INTERACTION_WILL_LOCK_EVENT";
const string USER_INTERACTION_WILL_UNLOCK_EVENT = "USER_INTERACTION_WILL_UNLOCK_EVENT";

const string FIRE_BUTTON_PRESSED_EVENT          = "FIRE_BUTTON_PRESSED_EVENT";
const string RELOAD_BUTTON_PRESSED_EVENT        = "RELOAD_BUTTON_PRESSED_EVENT";

// fonts
const string FONT_NEURON = "Corradine Fonts - NeuronBlack-Italic.otf";

#endif
