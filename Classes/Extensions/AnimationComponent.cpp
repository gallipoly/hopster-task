//
//  AnimationComponent.cpp
//

#include "AnimationComponent.h"

using namespace c2d;

AnimationComponent::AnimationComponent(const string& prefix, map<string, bool> loopInfo)
: _prefix(prefix),
  _loopInfo(loopInfo)
{
    init();
    _name = "Animation";
}

auto AnimationComponent::playAnimation(const string& name, bool reverse) const->void {
    auto animationName = _prefix + name;
    auto animation =  AnimationCache::getInstance()->getAnimation(name.c_str());
    if (!animation)
        throw make_shared<doesntExist>();
        
    auto looped = _loopInfo.at(animationName);
    auto action = reverse ? Animate::create(animation)->reverse() : Animate::create(animation);
    getOwner()->runAction(looped ? RepeatForever::create(action) : static_cast<Action*>(action));
}
