//
//  AnimationComponent.h
//

#ifndef _AnimationComponent__
#define _AnimationComponent__

#include "cocos-ext.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC_EXT;
USING_NS_CC;

using namespace std;

namespace c2d {
    
    class AnimationComponent : public Component {
    public:
        class doesntExist : public exception {};
    
        AnimationComponent(const string& prefix, map<string, bool> loopInfo);
        
        auto playAnimation(const string& name, bool reverse) const->void;
        
    protected:
        string                  _prefix;
        map<string, bool>       _loopInfo;
    };
}

#endif
