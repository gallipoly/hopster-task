//
//  AssetsLoader.cpp
//
//

#include "AssetsLoader.h"
#include "AssetsManager.h"
#include "DirectorHelper.h"
#include "FileManager.h"
#include "SimpleAudioEngine.h"


#define OBLOG(format, ...) cocos2d::log(format, ##__VA_ARGS__)


using namespace c2d;

enum {
    kSfx,
    kMusic,
    kTexture,
    kAtlasFrames,
    kAnimation,
    kAnimationName,
    // ------------
    kLast };


AssetsLoader::AssetsLoader(string file, function<void(size_t)> progressFn) {
    vector<string> filesPath;
    filesPath.push_back(file);
    _updateProgressFn = progressFn;
    _onCompleteFn = nullptr;
    
    init(filesPath);
}

AssetsLoader::AssetsLoader(vector<string> files, function<void(size_t)> progressFn) {
    _updateProgressFn = progressFn;
    _onCompleteFn = nullptr;
    init(files);
}

void AssetsLoader::init(const vector<string> files) {
    _loadedResCount = 0;
    _totalResCount = 0;
    _currentAsset = 0;
    _unloadResourcesOnDestroy = false;
    _filesList = files;
    
    map<int, set<string>> preAssets;
    
    Json::Reader reader;
    for (auto file : _filesList) {
        if (!AssetsManager::getInstance()->isAssetLoaded(file)) {
            //auto path = FileUtils::getInstance()->fullPathForFilename(file.c_str());
            auto data = FileUtils::getInstance()->getStringFromFile(file);
            Json::Value root;
            if (!reader.parse(data, root))
                throw make_shared<wrongFileParsing>();
                
            if (root.size() > 0) {
                _addAssets(root, preAssets);
                AssetsManager::getInstance()->addAsset(file);
            }
        }
    }
    
    if (preAssets.size() > 0) {
        if (_updateProgressFn)
            _updateProgressFn(0);
        
        _preload(preAssets);
        _scheduleFuncId = DirectorHelper::scheduleFunction(CC_CALLBACK_0(AssetsLoader::_loadOneResource, this), 0.f);
    }
    else {
        if (_updateProgressFn)
            _updateProgressFn(100);
        DirectorHelper::scheduleFunctionOnce(CC_CALLBACK_0(AssetsLoader::_onLoadingCompleted, this), 0.f);
    }
}

AssetsLoader::~AssetsLoader() {
    if (_unloadResourcesOnDestroy) {
        for (auto file : _filesList)
            AssetsManager::getInstance()->removeAsset(file);
            
        for (auto asset : _assets) {
            auto type = asset.first;
            auto file = asset.second;
            switch (type) {
                case kSfx:
                    OBLOG("Unloading sfx... %s", file.c_str());
                    CocosDenshion::SimpleAudioEngine::getInstance()->unloadEffect(file.c_str());
                    break;
                case kTexture:
                    OBLOG("Unloading texture... %s", file.c_str());
                    Director::getInstance()->getTextureCache()->removeTextureForKey(file.c_str());
                    break;
                case kAtlasFrames:
                    OBLOG("Unloading atlas frames... %s", file.c_str());
                    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile(file.c_str());
                    break;
                case kAnimationName:
                    OBLOG("Unloading animation... %s", file.c_str());
                    AnimationCache::getInstance()->removeAnimation(file.c_str());
                    break;
                default:
                    break;
            }
        }
    }
    OBLOG("~AssetsLoader: destroyed. Assets %s!", _unloadResourcesOnDestroy ? "unloaded" : "not unloaded");
}

void AssetsLoader::_updateProgress() {
    ++_loadedResCount;    
    if (_updateProgressFn) {
        auto per = 100 * _loadedResCount / _totalResCount;
        if (per > 100)
            OBLOG("ERROR: _totalResCount(%zu) > _loadedResCount(%zu)!", _totalResCount, _loadedResCount);
        _updateProgressFn(per);
    }
    if (_loadedResCount == _totalResCount) {
        DirectorHelper::unscheduleFunction(_scheduleFuncId);
        DirectorHelper::scheduleFunctionOnce(CC_CALLBACK_0(AssetsLoader::_onLoadingCompleted, this), 0.f);
    }
}

void AssetsLoader::_addAssets(const Json::Value& root, map<int, set<string>>& preassets) {
    auto music = root["music"];
    for (auto file : music)
        preassets[kMusic].insert(file.asString());
    
    auto sfx = root["sfx"];
    for (auto file : sfx)
        preassets[kSfx].insert(file.asString());
    
    auto textures = root["textures"];
    for (auto file : textures)
        preassets[kTexture].insert(file.asString());
    
    auto atlases = root["atlases"];
    for (auto file : atlases)
        preassets[kAtlasFrames].insert(file.asString());
    
    auto animations = root["animations"];
    for (auto file : animations)
        preassets[kAnimation].insert(file.asString());
}

void AssetsLoader::_preload(map<int, set<string> >& preassets) {
    const auto music = preassets[kMusic];
    for (auto mus : music) {
        auto musName = FileManager::getInstance().getSoundFullPathAndSuffix(mus);
        _assets.push_back(make_pair(kMusic, musName));
        ++_totalResCount;        
    }
    
    auto sfxs = preassets[kSfx];
    for (auto sfx : sfxs) {
        auto sfxName = FileManager::getInstance().getSoundFullPathAndSuffix(sfx);
        _assets.push_back(make_pair(kSfx, sfxName));
         ++_totalResCount;
    }
    
    set<string> allTextures;
    auto textures = preassets[kTexture];
    for (auto file : textures) {
        auto texName = FileManager::getInstance().getTextureNameWithSuffix(file);
        allTextures.insert(texName);
    }
    
    set<string> allAtlases;
    auto atlases = preassets[kAtlasFrames];
    for (auto file : atlases) {
        auto texName = FileManager::getInstance().getTextureNameWithSuffix(file);
        allTextures.insert(texName);
        auto plistName = file + ".plist";
        allAtlases.insert(plistName);
    }
    
    set<string> allAnimations;
    auto animations = preassets[kAnimation];
    for (auto file : animations) {
        auto plistName = file + ".plist";
        allAnimations.insert(plistName);
        
        auto path = FileUtils::getInstance()->fullPathForFilename(plistName.c_str());
        auto dict = FileUtils::getInstance()->getValueMapFromFile(path.c_str());
        if (dict.empty())
            throw make_shared<wrongObjectCreation>();
        
        auto properties = dict.at("properties").asValueMap();
        if (!properties.empty()) {
            auto spritesheets = properties.at("spritesheets").asValueVector();
            for (auto spsh : spritesheets) {
                string name = spsh.asString();
                allAtlases.insert(name);
                
                name = name.substr(0, name.find_last_of('.'));
                name = FileManager::getInstance().getTextureNameWithSuffix(name);
                allTextures.insert(name);
            }
        }
        
        auto anims = dict.at("animations").asValueMap();
        if (!anims.empty())
            for (auto anim : anims)
                _assets.push_back(make_pair(kAnimationName, anim.first));
    }
    
    vector<pair<string, size_t>> texturesAndSizes;
    for (auto tex : allTextures) {
        texturesAndSizes.push_back(make_pair(tex, FileManager::getInstance().getFileSize(tex)));
    }
    sort(texturesAndSizes.begin(), texturesAndSizes.end(), [](const pair<string, size_t>& i, const pair<string, size_t>& j) { return i.second > j.second; });
    
    for (auto tex : texturesAndSizes) {
        _assets.push_back(make_pair(kTexture, tex.first));
        ++_totalResCount;
    }
    
    for (auto atlas : allAtlases) {
        _assets.push_back(make_pair(kAtlasFrames, atlas));
        ++_totalResCount;
    }
    
    for (auto anim : allAnimations) {
        _assets.push_back(make_pair(kAnimation, anim));
        ++_totalResCount;
    }
}

void AssetsLoader::_loadOneResource() {
    if (_currentAsset >= _assets.size())
        return;
    
    auto type = _assets[_currentAsset].first;
    auto file = _assets[_currentAsset].second;
    
    switch (type) {
        case kMusic:
            CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(file.c_str());
            break;
        case kSfx:
            CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect(file.c_str());
            break;
        case kTexture:
            Director::getInstance()->getTextureCache()->addImage(file.c_str());
            break;
        case kAtlasFrames:
            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(file.c_str());
            break;
        case kAnimation:
            AnimationCache::getInstance()->addAnimationsWithFile(file.c_str());
            break;
        case kAnimationName:
            _currentAsset++;
            return;
            
        default:
            break;
    }
    
    OBLOG("Loading... %s", file.c_str());
    _updateProgress();
    _currentAsset++;
}
