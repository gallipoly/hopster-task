//
//  AssetsLoader.h
//

#ifndef _AssetsLoader__
#define _AssetsLoader__

#include "cocos2d.h"
#include <json/json.h>

USING_NS_CC;
using namespace std;

namespace c2d {
    
    class AssetsLoader {
    friend class ScheduleObject;
    public:
        class wrongFileParsing : public exception {};
        class objectDoesntExist : public exception {};
        class wrongObjectCreation : public exception {};
    
        AssetsLoader(vector<string> filesPath, function<void(size_t)> progressFn = nullptr);
        AssetsLoader(string filePath, function<void(size_t)> progressFn = nullptr);
        virtual ~AssetsLoader();
        
        inline auto setOnCompleteHandler(function<void(void)> handler)->void    { _onCompleteFn = handler; }
        inline auto setUnloadResourcesOnDestroy(bool flag)->void                { _unloadResourcesOnDestroy = flag; }
        
    protected:
        auto init(const vector<string> filesPath)->void;
        auto _updateProgress()->void;
        auto _addAssets(const Json::Value& root, map<int, set<string>>& preassets)->void;
        auto _preload(map<int, set<string> >& preassets)->void;
        auto _loadOneResource()->void;
        auto _onLoadingCompleted()->void    { if (_onCompleteFn) _onCompleteFn(); }
        
    protected:
        vector<pair<int, string>>                       _assets;
        int                                             _currentAsset;
        
        vector<string>                                  _filesList;
        
        size_t                                          _loadedResCount;
        size_t                                          _totalResCount;
        
        int                                             _scheduleFuncId;
        
        function<void(size_t)>                          _updateProgressFn;
        function<void(void)>                            _onCompleteFn;
        
        bool                                            _unloadResourcesOnDestroy;
    };
}

#endif
