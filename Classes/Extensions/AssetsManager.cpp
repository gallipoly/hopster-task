//
//  AssetsManager.cpp
//

#include "AssetsManager.h"


using namespace c2d;


static AssetsManager* _instance = nullptr;

AssetsManager* AssetsManager::getInstance() {
    if (_instance == nullptr)
        _instance = new AssetsManager();
    
    return _instance;
}

AssetsManager::AssetsManager() {}

void AssetsManager::addAsset(const string& file) {
    if (!isAssetLoaded(file)) {
        _loadedAssets.insert(file);
    }
    else {
        CCLOG("ERROR: AssetsManager: asset [%s] already added!", file.c_str());
    }
}
void AssetsManager::removeAsset(const string& file) {
    if (isAssetLoaded(file)) {
        _loadedAssets.erase(file);
    }
    else {
        CCLOG("ERROR: AssetsManager: asset [%s] not found!", file.c_str());
    }
}

bool AssetsManager::isAssetLoaded(const string& file) {
    auto it = _loadedAssets.find (file);
    if (it != _loadedAssets.end()) {
        return true;
    }
    return false;
}

void AssetsManager::printInfo() {
    CCLOG("INFO: AssetsManager: Loaded assets list:");
    for (auto file : _loadedAssets) {
        CCLOG("asset - [%s]", file.c_str());
    }
}
