//
//  AssetsManager.h
//

#ifndef __BubbleSplash__AssetsManager__
#define __BubbleSplash__AssetsManager__

#include "cocos2d.h"

namespace c2d {
    
    class AssetsManager {
    public:
        static auto getInstance()->AssetsManager*;
        
        auto addAsset(const string& file)->void;
        auto removeAsset(const string& file)->void;
        
        auto isAssetLoaded(const string& file)->bool;
        
        auto printInfo()->void;
        
    private:
        AssetsManager();
        
        set<string> _loadedAssets;
    };
}

#endif /* defined(__AssetsManager__) */
