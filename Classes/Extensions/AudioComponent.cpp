//
//  AudioComponent.cpp
//

#include "AudioComponent.h"

using namespace c2d;

AudioComponent::AudioComponent() {
    ComAudio::init();
}

auto AudioComponent::addSoundEvent(const string& name, const string& fileName, bool looped)->void {
    _sfxEvents.insert(make_pair(name, make_pair(fileName, looped)));
}

auto AudioComponent::addBackgroundSoundEvent(const string& name, const string& fileName, bool looped, float volume)->void {
    _backEvents.insert(make_pair(name, make_pair(fileName, make_pair(looped, volume))));
}

auto AudioComponent::playEvent(const string& name)->void {
    auto it = _sfxEvents.find(name);
    if (it != _sfxEvents.end()) {
        playEffect(it->second.first.c_str(), it->second.second);
    }
}

auto AudioComponent::playBackgroundEvent(const string& name)->void {
    auto it = _backEvents.find(name);
    if (it != _backEvents.end()) {
        setBackgroundMusicVolume(it->second.second.second);
        playBackgroundMusic(it->second.first.c_str(), it->second.second.first);
    }
}
