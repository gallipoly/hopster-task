//
//  AudioComponent.h
//

#ifndef _AudioComponent__
#define _AudioComponent__

#include "cocos-ext.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC_EXT;
using namespace std;

namespace c2d {
    
    class AudioComponent : public cocostudio::ComAudio {
    public:
        AudioComponent();
        
        auto addSoundEvent(const string& name, const string& fileName, bool looped)->void;
        auto addBackgroundSoundEvent(const string& name, const string& fileName, bool looped, float volume)->void;
        
        auto playEvent(const string& name)->void;
        auto playBackgroundEvent(const string& name)->void;
    
    protected:
        map<string, pair<string, bool>>                 _sfxEvents;
        map<string, pair<string, pair<bool, float>>>    _backEvents;
    };
}

#endif
