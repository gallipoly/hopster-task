//
//  BasicPopup.cpp
//

#include "BasicPopup.h"
#include "DirectorHelper.h"

using namespace c2d;

static const float ANIMATION_TIME_1 = .15f;
static const float ANIMATION_TIME_2 = .2f;

static int globalZOrder = 100000;

BasicPopup::BasicPopup(shared_ptr<AssetsLoader> builder, const string& name)
: _builder(builder)
, _onCloseCallback(nullptr)
, _onKeyBackCallback(nullptr)
{
    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(BasicPopup::onTouchBegan, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(BasicPopup::onTouchEnded, this);
    touchListener->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    auto keyboardListener = EventListenerKeyboard::create();
    keyboardListener->onKeyReleased = CC_CALLBACK_2(BasicPopup::onKeyReleased, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);
    
    init(name); // TODO: make normal call
}

bool BasicPopup::init(const string& name) {
    if (Layer::init()) {
        setName(name);
        
        _scrSize = Director::getInstance()->getWinSize();
        
        setAnchorPoint(Point(.5f, .5f));
        
        _fadingSprite = LayerColor::create(Color4B(0, 0, 0, 0));
        addChild(_fadingSprite, -1);
        
        _scalingNode = Node::create();
        _scalingNode->setPosition(Point(_scrSize.width / 2.f, _scrSize.height / 2.f));
        addChild(_scalingNode);
        
        _scaledNode = Node::create();
        _scaledNode->setPosition(Point(-_scrSize.width / 2.f, -_scrSize.height / 2.f));
        _scalingNode->addChild(_scaledNode);
        
        return true;
    }
    
    return false;
}

void BasicPopup::show(int zOrder /*= -1*/) {
    auto parent = Director::getInstance()->getRunningScene();
    parent->addChild(this, zOrder >= 0 ? zOrder : globalZOrder++);
    
    _scalingNode->setScale(.7f);
    _scalingNode->runAction(Sequence::createWithTwoActions(ScaleTo::create(ANIMATION_TIME_1, 1.15f),
                                                           ScaleTo::create(ANIMATION_TIME_2, 1.f)));
    _fadingSprite->runAction(Sequence::createWithTwoActions(FadeTo::create(ANIMATION_TIME_1 + ANIMATION_TIME_2, 100),
                                                            CallFunc::create([&, this] { setTouchEnabled(true); })));
}

void BasicPopup::dismiss() {
    setTouchEnabled(false);
    _scalingNode->runAction(Sequence::createWithTwoActions(ScaleTo::create(ANIMATION_TIME_1, 1.15f), ScaleTo::create(ANIMATION_TIME_2, 0.f)));
    _fadingSprite->stopAllActions();
    _fadingSprite->runAction(Sequence::createWithTwoActions(FadeTo::create(ANIMATION_TIME_1 + ANIMATION_TIME_2, 0), CallFunc::create([&, this] {
        removeFromParent();
        if (_onCloseCallback)
            c2d::DirectorHelper::scheduleFunctionOnce(_onCloseCallback, 0);
    })));
}

bool BasicPopup::isTouchEnabled() {
    return  _isTouchesEnabled;
}

void BasicPopup::setTouchEnabled(bool enabled) {
    _isTouchesEnabled = enabled;
    for (auto child : _children) {
        Menu* tmpMenu = dynamic_cast<Menu*>(child);
        if (tmpMenu != nullptr)
            tmpMenu->setEnabled(enabled);
    }
}

bool BasicPopup::onTouchBegan(Touch* pTouch, Event* pEvent) {
    return true;
}

void BasicPopup::onTouchEnded(Touch* pTouch, Event* pEvent) {
}

void BasicPopup::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event) {
    if (EventKeyboard::KeyCode::KEY_BACK == keyCode) {
        if (isTouchEnabled()) {
            if(_onKeyBackCallback)
                _onKeyBackCallback();
        }
    }
}
