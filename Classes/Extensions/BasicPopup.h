//
//  BasicPopup.h
//

#ifndef __BasicPopup__
#define __BasicPopup__

#include "cocos2d.h"
#include "AssetsLoader.h"

USING_NS_CC;
using namespace std::placeholders;

namespace c2d {
    
    class BasicPopup : public Layer {
    public:
        BasicPopup(shared_ptr<AssetsLoader> builder, const string& name);
        
        virtual auto init(const string& name)->bool;
        
        virtual auto onTouchBegan(Touch*, Event*)->bool;
        virtual auto onTouchEnded(Touch*, Event*)->void;
        virtual auto onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)->void;
        
        virtual auto show(int zOrder = -1)->void;
        virtual auto dismiss()->void;
        
        virtual auto isTouchEnabled()->bool;
        virtual auto setTouchEnabled(bool enabled)->void;
        
        inline  auto setOnCloseHandler(function<void(void)> handler)->void { _onCloseCallback = handler; }
        
    protected:
        shared_ptr<AssetsLoader>    _builder;
        
        function<void(void)>        _onCloseCallback;
        function<void(void)>        _onKeyBackCallback;
        
        LayerColor*                 _fadingSprite;
        Node*                       _scaledNode;        //node that's positioned in the bottom-left corner
        Node*                       _scalingNode;       //node that's positioned in the screen center and is being scaled
        
        Size                        _scrSize;
        
        bool                        _isTouchesEnabled;    
    };
}

#endif /* defined(__BasicPopup__) */
