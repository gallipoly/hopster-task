//
//  ComponentActions.cpp
//

#include "ComponentActions.h"
#include "AudioComponent.h"
#include "AnimationComponent.h"

using namespace c2d;

auto PlaySfx::initWithName(string name)->bool {
    _sfxName = name;
    return true;
}

auto PlaySfx::create(string name)->PlaySfx* {
    PlaySfx* pRet = new PlaySfx();
    if (pRet && pRet->initWithName(name)) {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return nullptr;
}

auto PlaySfx::step(float time)->void {
    auto audio = getTarget()->getComponent("Audio");
    if (audio) static_cast<AudioComponent*>(audio)->playEvent(_sfxName);
}

auto PlaySfx::clone() const->Action* {
    auto a = new PlaySfx();
	a->initWithName(_sfxName);
	a->autorelease();
	return  a;
}

auto PlaySfx::reverse() const->Action* {
    return clone();
}

// ----------------------------------------------------

auto PlayAnimation::initWithName(string name)->bool {
    _playReverse = false;
    _animName = name;
    return true;
}

auto PlayAnimation::create(string name)->PlayAnimation* {
    PlayAnimation* pRet = new PlayAnimation();
    if (pRet && pRet->initWithName(name)) {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return nullptr;
}

auto PlayAnimation::step(float time)->void {
    auto anim = getTarget()->getComponent("Animation");
    if (anim) static_cast<AnimationComponent*>(anim)->playAnimation(_animName, _playReverse);
}

auto PlayAnimation::clone() const->Action* {
    auto a = new PlayAnimation();
	a->initWithName(_animName);
	a->autorelease();
	return  a;
}

auto PlayAnimation::reverse() const->Action* {
    auto a = new PlayAnimation();
	a->initWithName(_animName);
    a->_playReverse = !_playReverse;
	a->autorelease();
	return  a;
}
