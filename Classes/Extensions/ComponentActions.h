//
//  ComponentActions.h
//

#ifndef _ComponentActions__
#define _ComponentActions__

#include "cocos2d.h"

USING_NS_CC;
using namespace std;

namespace c2d {
    
    class PlaySfx : public Action {
    public:
        auto initWithName(string name)->bool;
        static auto create(string name)->PlaySfx*;

        virtual auto step(float time)->void override;
        virtual auto clone() const->Action* override;
        virtual auto reverse() const->Action* override;
        
    protected:
        string  _sfxName;
    };
    
    class PlayAnimation : public Action {
    public:
        auto initWithName(string name)->bool;
        static auto create(string name)->PlayAnimation*;
        
        virtual auto step(float time)->void override;
        virtual auto clone() const->Action* override;
        virtual auto reverse() const->Action* override;
        
    protected:
        string  _animName;
        bool    _playReverse;
    };
}

#endif
