//
//  DirectorHelper.cpp
//

#include "DirectorHelper.h"

USING_NS_CC;

namespace c2d {
    class ScheduleObject : public Ref {
        function<void(void)> _callback;
    public:
        ScheduleObject(function<void(void)> callback) : _callback(callback) {}
        auto call(float)->void { if (_callback) _callback(); }
    };
}

static int _objectId = 0;
static map<int, shared_ptr<c2d::ScheduleObject>> _scheduledObjects;


int c2d::DirectorHelper::scheduleFunction(function<void(void)> func, float fInterval) {
    return DirectorHelper::scheduleFunction(func, fInterval, kRepeatForever, 0.f);
}

int c2d::DirectorHelper::scheduleFunction(function<void(void)> func, float fInterval, unsigned int repeat, float delay) {
    int iD = _objectId++;
    auto sch_obj = _shared(new ScheduleObject(func));
    _scheduledObjects[iD] = sch_obj;
    Director::getInstance()->getScheduler()->schedule(schedule_selector(ScheduleObject::call), sch_obj.get(), fInterval, repeat, delay, false);
    return iD;
}

void c2d::DirectorHelper::scheduleFunctionOnce(function<void(void)> func, float delay) {
    int iD = _objectId;
    auto callFunc = [func, iD](){
        if (func)
            func();
        DirectorHelper::unscheduleFunction(iD);
    };
    DirectorHelper::scheduleFunction(callFunc, 0.f, 0, delay);
}

void c2d::DirectorHelper::unscheduleFunction(int iD) {
    auto it = _scheduledObjects.find(iD);
    if (it != _scheduledObjects.end()) {
        Director::getInstance()->getScheduler()->unschedule(schedule_selector(ScheduleObject::call), it->second.get());
        _scheduledObjects.erase(it);
    }
}
