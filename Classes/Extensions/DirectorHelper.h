//
//  DirectorHelper.h
//

#ifndef __DirectorHelper__
#define __DirectorHelper__

#include "cocos2d.h"

using namespace std;
using std::function;

namespace c2d {
    namespace DirectorHelper {
        auto scheduleFunction(function<void(void)> func, float fInterval)->int;
        auto scheduleFunction(function<void(void)> func, float fInterval, unsigned int repeat, float delay)->int;
        auto scheduleFunctionOnce(function<void(void)> func, float delay)->void;
        auto unscheduleFunction(int iD)->void;
    }
}

#endif /* defined(__DirectorHelper__) */
