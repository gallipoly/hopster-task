//
//  FileDecrypter.cpp
//

#include "FileDecrypter.h"
#include "base/base64.h"

using namespace c2d;

auto FileDecrypter::_xorData(string data, const string& xKey)->unsigned char* {
    auto dataSize = data.size();
    unsigned char *result = (unsigned char *)malloc(sizeof(unsigned char) * (dataSize + 1));
    result[dataSize] = 0;
    
    unsigned char *p = result;
    static auto keyLength = xKey.length();
    
    auto x = 0;
    for (auto &ch : data) {
        *(p++) = ch ^ xKey[x];
        if (++x == keyLength)
            x = 0;
    }
    return result;
}

auto FileDecrypter::encrypt(string &data, const string& xKey)->string {
    char *result;
    auto xorDat = _xorData(data, xKey);
    cocos2d::base64Encode((unsigned char*)xorDat, (unsigned int)data.length(), &result);
    free(xorDat);
    
    return result;
}

auto FileDecrypter::decrypt(string &data, const string& xKey)->string {
    unsigned char *base64;
    cocos2d::base64Decode((unsigned char*)data.c_str(), (unsigned int)data.length(), &base64);
    auto noXor = _xorData((char *)base64, xKey);
    auto result = string((char *)noXor);
    free(noXor);
    
    return result;
}


