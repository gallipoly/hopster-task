//
//  FileDecrypter.h
//

#ifndef __FileDecrypter__
#define __FileDecrypter__

using namespace std;

namespace c2d {
    class FileDecrypter {
    public:
        static string encrypt(string &data, const string& xKey);
        static string decrypt(string &data, const string& xKey);

    private:
        static unsigned char* _xorData(string data, const string& xKey);
        FileDecrypter() {};
    };
}

#endif /* defined(__FileDecrypter__) */
