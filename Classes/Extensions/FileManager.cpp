//
//  FileHelper.cpp
//

#include "cocos2d.h"
#include "FileManager.h"
#include "FileDecrypter.h"

USING_NS_CC;
using namespace c2d;

auto FileManager::getInstance()->FileManager& {
    static FileManager sInstance;
    return sInstance;
}

auto FileManager::isFileExist(const string& fileName) const->bool {
    return FileUtils::getInstance()->isFileExist(fileName);
}

auto FileManager::getFileSize(const string& filePath) const->size_t {
    size_t size = 0;
    CCASSERT(!filePath.empty(), "Invalid parameters.");
    do {
        auto fullPath = FileUtils::getInstance()->fullPathForFilename(filePath.c_str());
        // read the file from hardware
        FILE *fp = fopen(fullPath.c_str(), "rb");
        CC_BREAK_IF(!fp);
        
        fseek(fp,0,SEEK_END);
        size = ftell(fp);
        fclose(fp);
    } while (0);
    
    return size;
}

auto FileManager::writeStringToFile(const string& str, const string& fileName, const string& encryptKey)->void {
    auto fileContent = str;
    if (!encryptKey.empty()) {
        fileContent = FileDecrypter::decrypt(fileContent, encryptKey);
    }
};

auto FileManager::getTextureNameWithSuffix(const string& name) const->string {
    return name + ".png";
}

auto FileManager::getSoundFullPathAndSuffix(const string& fileNameWithoutExtension) const->string {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    return fileNameWithoutExtension + ".caf";
    
#elif  (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    return fileNameWithoutExtension + ".wav";
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    return fileNameWithoutExtension + ".ogg";
    
#endif
}


