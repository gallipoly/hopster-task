//
//  FileHelper.h
//

#ifndef __FileManager__
#define __FileManager__

using namespace std;

namespace c2d {
    
    class FileManager {
    public:
        static auto getInstance()->FileManager&;
    
        auto isFileExist(const string& fileName) const->bool;
        auto getFileSize(const string& filePath) const->size_t;
        auto writeStringToFile(const string& str, const string& filePath, const string& encryptKey = "")->void;
        auto getTextureNameWithSuffix(const string& name) const->string;
        auto getSoundFullPathAndSuffix(const string& fileNameWithoutExtension) const->string;
    };
}

#endif
