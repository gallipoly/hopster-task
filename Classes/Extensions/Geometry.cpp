//
//  Geometry.cpp
//

#include "Geometry.h"

using namespace c2d::geometry;

float sign(const cocos2d::Vec2 &p1, const cocos2d::Vec2 &p2, const cocos2d::Vec2 &p3) {
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

float linePointDistance(cocos2d::Vec2& begin, cocos2d::Vec2& end, cocos2d::Vec2& point, bool isSegment) {
    if (begin == end)
        CCLOG("ERROR: wrong line segment definition!!!");
    const float l2 = begin.distanceSquared(end) ;
    if (l2 == 0.f)
        return point.distance(begin);
    const float t = Vec2::dot(point - begin, end - begin) / l2;
    if (t < 0.f)
        return point.distance(begin);
    else if (t > 1.f)
        return point.distance(end);
    const cocos2d::Vec2 projection = begin + t * (end - begin);
    return point.distance(projection);
}


bool c2d::geometry::isPointInPolygon(const cocos2d::Vec2 *verts, int count, const cocos2d::Vec2 &point) {
    bool isInside = false;
    int i, j;
    for (i = 0, j = count-1; i < count; j = i++) {
        if ( ((verts[i].y>point.y) != (verts[j].y>point.y)) &&
            (point.x < (verts[j].x-verts[i].x) * (point.y-verts[i].y) / (verts[j].y-verts[i].y) + verts[i].x) )
            isInside = !isInside;
    }
    return isInside;
}

bool c2d::geometry::isPointInTriangle(const cocos2d::Vec2 &pt, const cocos2d::Vec2 &v1, const cocos2d::Vec2 &v2, const cocos2d::Vec2 &v3) {
    bool b1, b2, b3;
    b1 = sign(pt, v1, v2) < 0.0f;
    b2 = sign(pt, v2, v3) < 0.0f;
    b3 = sign(pt, v3, v1) < 0.0f;
    return ((b1 == b2) && (b2 == b3));
}

auto c2d::geometry::isPointInsideAnyShape(vector<Polygon>& shapes, Point pt)->bool {
    for (auto& shape : shapes)
        if (isPointInPolygon(&shape[0], (int)shape.size(), pt))
            return true;
    return false;
}

auto c2d::geometry::isCircleInsideAnyShape(vector<Polygon>& shapes, Circle circle)->bool {
    if (!circle.isValid())
        return false;
    for (auto& shape : shapes)
        if (isPointInPolygon(&shape[0], (int)shape.size(), circle.position))
            return true;
    for (auto i = 0; i < (int)shapes.size(); ++i) {
        auto shape = shapes[i];
        for (auto j = -1; j < (int)shape.size() - 1; ++j) {
            auto dist = 0;
            if (-1 == j)
                dist = linePointDistance(shape.back(), shape[0], circle.position, true);
                else
                    dist = linePointDistance(shape[j], shape[j+1], circle.position, true);
                    
                    if (dist <= circle.radius)
                        return true;
        }
    }
    return false;
}
