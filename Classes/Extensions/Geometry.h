//
//  Geometry.h
//

#ifndef Geometry_h
#define Geometry_h

#include "cocos2d.h"

USING_NS_CC;
using namespace std;

namespace c2d {
    namespace geometry {
        
        typedef vector<cocos2d::Point>      Polygon;
        
        struct Circle
        {
            Circle() : position(cocos2d::Point::ZERO), radius(0.f) {}
            Circle(cocos2d::Point  p, float r) : position(p), radius(r) {}
            
            bool isValid() { return radius > 0.f; }
            
            cocos2d::Point  position;
            float           radius;
        };
        
        
        auto isPointInsideAnyShape(vector<Polygon>& shapes, Point pt)->bool;
        auto isPointInPolygon(const Point *verts, int count, const Point &point)->bool;
        auto isPointInTriangle(const Point &pt, const Point &v1, const Point &v2, const Point &v3)->bool;
        auto isCircleInsideAnyShape(vector<Polygon>& shapes, Circle circle)->bool;
    }
}

#endif /* Geometry_h */
