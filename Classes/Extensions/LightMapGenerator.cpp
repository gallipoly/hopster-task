//
//  LightMapGenerator.cpp
//

#include "LightMapGenerator.h"
#include "PolygonTriangulation.h"


LightMapGenerator* LightMapGenerator::create(Rect borders) {
    LightMapGenerator *ret = new (std::nothrow) LightMapGenerator(borders);
    if (ret && ret->init())
    {
        ret->autorelease();
        return ret;
    }
    else
    {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}

LightMapGenerator::LightMapGenerator(Rect borders) {
    _border = borders;
	_corners.push_back(Point(_border.origin.x, _border.origin.y));
	_corners.push_back(Point(_border.origin.x, _border.size.height));
	_corners.push_back(Point(_border.size.width, _border.size.height));
	_corners.push_back(Point(_border.size.width, _border.origin.y));
    
    _lightMap = _shared_auto(DrawNode::create());
}

auto LightMapGenerator::removeShape(vector<Point> shape)->void {
    auto remove_index = -1;
    for (auto i = 0; i < _shapes.size(); ++i) {
        if (_shapes[i] == shape) {
            remove_index = i;
            break;
        }
    }
    if (remove_index != -1) {
        _shapes.erase (_shapes.begin() + remove_index);
    }
}

auto LightMapGenerator::isPointInsideAnyShadow(Point& pt)->bool {
    return c2d::geometry::isPointInsideAnyShape(_shadows, pt);
}

auto LightMapGenerator::isCircleInsideAnyShadow(Circle& circle)->bool {
    return c2d::geometry::isCircleInsideAnyShape(_shadows, circle);
}

auto LightMapGenerator::isShapeInsideAnyShadow(Polygon& shape)->bool {
    for (auto pt : shape) {
        if (!isPointInsideAnyShadow(pt))
            return false;
    }
    return true;
}

auto LightMapGenerator::resetLight()->void {
    _lightMap->clear();
    _shadows.clear();
    _shadows.push_back(_corners);
}

auto LightMapGenerator::updateLight(Point lightPos)->void {

    _lightMap->clear();
    _shadows.clear();
    
    // find shadow figures
    for (auto obstaclePoints : _shapes) {
        // find left and right last points of polygon //////////////////////////////////////
        int startIndex, endIndex;
        float maxDiffAngle = 0.f;
		for (auto n = 0; n < obstaclePoints.size() - 1; ++n) {
            for (int m = n + 1; m < obstaclePoints.size(); ++m) {
                Point a = obstaclePoints[n];
                Point b = obstaclePoints[m];
                a.subtract(lightPos);
                b.subtract(lightPos);
                float angle = fabsf(a.getAngle() - b.getAngle());
                angle = (angle < M_PI) ? angle : 2.f * M_PI - angle;
                if (angle > maxDiffAngle) {
                    maxDiffAngle = angle;
                    startIndex = n;
                    endIndex = m;

                }
            }
        }
        // find intersections left and right light rays with screen border //////////////////
        Point left = obstaclePoints[startIndex];
        Point right = obstaclePoints[endIndex];
#ifdef DEBUG_SHAPES
        if(_debugNode) {
            _debugNode->drawDot(left, 5.f, Color4F(1.0, 0.0, 0.0, 1.0));
            _debugNode->drawDot(right, 5.f, Color4F(0.0, 1.0, 0.0, 1.0));
        }
#endif
        Point border_left = _getIntersectionWithBorder(_border, lightPos, left);
        Point border_right = _getIntersectionWithBorder(_border, lightPos, right);
        
        // find border corner points within left and right light rays ///////////////////////
        left -= lightPos;
        right -= lightPos;
        left.normalize();
        right.normalize();
        float mult = 10000000.f;
        left *= mult;
        right *= mult;
        left += lightPos;
        right += lightPos;
#ifdef DEBUG_SHAPES
        if(_debugNode)
            _debugNode->drawTriangle(lightPos, left, right, Color4F(0.5, 0.5, 0.5, 0.4)); // debug
#endif
        Polygon cornerPoints;
        for(auto cor : _corners) {
            if (c2d::geometry::isPointInTriangle(cor, lightPos, left, right))
                cornerPoints.push_back(cor);
        }

        if (cornerPoints.size() > 1) {
            if (Point::isSegmentIntersect(border_left, cornerPoints.back(), border_right, cornerPoints.front())) {
                reverse(cornerPoints.begin(),cornerPoints.end());
            }
        }
                
        // build shadow polygon /////////////////////////////////////////////////////////////
        Polygon shadowPolygon;
		shadowPolygon.push_back(border_left);
		for (auto j = startIndex; j <= endIndex; ++j) {
            shadowPolygon.push_back(obstaclePoints[j]);
        }
        shadowPolygon.push_back(border_right);
        
        shadowPolygon.reserve(shadowPolygon.size() + cornerPoints.size());
        shadowPolygon.insert(shadowPolygon.end(), cornerPoints.begin(), cornerPoints.end());
#ifdef DEBUG_SHAPES
        if(_debugNode)
            _debugNode->drawPoly(&shadowPolygon[0], (int)shadowPolygon.size(), false, Color4F(1,1,0,1)); // debug
#endif
        _shadows.push_back(shadowPolygon);
        
        // triangulate shadow polygon /////////////////////////////////////////////////////////
        Polygon result;
        Triangulate::Process(shadowPolygon, result);
        
        // draw shadow polygon ////////////////////////////////////////////////////////////////
        int tcount = (int)result.size() / 3;
		for (auto j = 0; j < tcount; ++j)
        {
            const Point &p1 = result[j*3+0];
            const Point &p2 = result[j*3+1];
            const Point &p3 = result[j*3+2];
            _lightMap->drawTriangle(p1, p2, p3, Color4F(0, 0, 0, 1));
        }
    }
    
    // draw shapes
    for(auto i = 0; i < _shapes.size(); ++i)
        _lightMap->drawPolygon(&_shapes[i][0], (int)_shapes[i].size(), Color4F(0,0,0,1), 1.f, Color4F(0,0,0,1));
}

auto LightMapGenerator::_getIntersectionWithBorder(const Rect& border, const Point& start, const Point& finish)->Point { // Point& start - segment start / Point& finish - segment finish
    vector<Point> intersectionPoints;
    if (finish.x > start.x)
        intersectionPoints.push_back(Vec2::getIntersectPoint(start, finish, Point(border.size.width, border.size.height), Point(border.size.width, border.origin.y)));  // left border
    else if (finish.x < start.x)
        intersectionPoints.push_back(Vec2::getIntersectPoint(start, finish, Point(border.origin.x, border.origin.y), Point(border.origin.x, border.size.height)));      // right border
    if (finish.y > start.y)
        intersectionPoints.push_back(Vec2::getIntersectPoint(start, finish, Point(border.origin.x, border.size.height), Point(border.size.width, border.size.height))); // top border
    else if (finish.y < start.y)
        intersectionPoints.push_back(Vec2::getIntersectPoint(start, finish, Point(border.size.width, border.origin.y), Point(border.origin.x, border.origin.y)));       // bottom border
    
    if (intersectionPoints.size()) {
        float closerDist = LONG_MAX;
        Point closerPoint;
        for(auto point : intersectionPoints) {
            float dist = start.distance(point);
            if (dist < closerDist) {
                closerDist = dist;
                closerPoint = point;
            }
        }
        return closerPoint;
    }
    return Point::ZERO;
}
