//
//  LightMapGenerator.h
//

#ifndef __LightMapGenerator__
#define __LightMapGenerator__

#include "cocos2d.h"
#include "Geometry.h"

USING_NS_CC;
using namespace c2d::geometry;

class LightMapGenerator : public Node {
public:
    
    LightMapGenerator(Rect borders);
    static LightMapGenerator* create(Rect borders);
    
    auto setShapes(vector<vector<Point>> shapes)->void { _shapes = shapes; }
    auto removeShape(vector<Point> shape)->void;
    auto getShadowsNode()->Node* { return (Node*)_lightMap.get(); }
    
    auto isPointInsideAnyShadow(Point& pt)->bool;
    auto isCircleInsideAnyShadow(Circle& circle)->bool;
    auto isShapeInsideAnyShadow(Polygon& shape)->bool;
    
    auto updateLight(Point lightPos)->void;
    auto resetLight()->void;
    
#ifdef DEBUG_SHAPES
    auto setDebugNode(DrawNode* node)->void { _debugNode = node; }
#endif
    
protected:
    auto _getIntersectionWithBorder(const Rect& border, const Point& start, const Point& finish)->Point;
    
protected:
    Rect                    _border;
    shared_ptr<DrawNode>    _lightMap;
    vector<vector<Point>>   _shapes;
    vector<vector<Point>>   _shadows;    
    vector<Point>           _corners;
    
#ifdef DEBUG_SHAPES
    DrawNode*               _debugNode;
#endif
};

#endif /* defined(__LightMapGenerator__) */
