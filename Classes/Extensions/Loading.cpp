//
//  LoadingScene.cpp
//

#include "Loading.h"

using namespace c2d;

Loading::Loading() {
    auto size = Director::getInstance()->getVisibleSize();
    
    initWithColor(Color4B(0, 0, 0, .7f), size.width, size.height);
    
	_label = _shared_auto(Label::createWithSystemFont("0%", "Arial", 48));
    _label->setPosition(Point(.5f*size.width, .5f*size.height));
    addChild(_label.get());
}

auto Loading::stepProgress(size_t percent)->void {
    _label->setString((to_string(percent) + "%").c_str());
    
    if (0 == percent) {
        retain();
        
        if (getParent()) {
            getParent()->addChild(this, numeric_limits<int>::max());
        } else {
            auto loaderScene = Scene::create();
            loaderScene->addChild(this);
            
            if (Director::getInstance()->getRunningScene())
                Director::getInstance()->replaceScene(loaderScene);
            else
                Director::getInstance()->runWithScene(loaderScene);
        }
    } else if (100 <= percent) {
        Director::getInstance()->getScheduler()->schedule(schedule_selector(Loading::_finishLoading), this, 0, 0, 0.5f, false);
    }
}

auto Loading::_finishLoading(float dt)->void {
    if (getParent()) {
        removeFromParentAndCleanup(true);
    }
    
    if (_finishLoadingFn) {
        _finishLoadingFn();
    }
    
    release();
}
