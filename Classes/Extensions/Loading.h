//
//  LoadingScene.h
//

#ifndef _Loading__
#define _Loading__

#include "cocos2d.h"

USING_NS_CC;
using namespace std;

namespace c2d {
    
    class Loading : public LayerColor {
    public:
        CREATE_FUNC(Loading);
        
        auto stepProgress(size_t percent)->void;
        auto setFinishHandler(function<void()> cb)->void { _finishLoadingFn = cb; }
    
    protected:
        Loading();
        
        auto _finishLoading(float dt)->void;
        
        shared_ptr<Label>       _label = nullptr;
        function<void()>        _finishLoadingFn = nullptr;
    };
}

#endif
