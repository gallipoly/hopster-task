//
//  MenuItemImageScale.cpp
//

#include "MenuItemImageScale.h"


MenuItemImageScale* MenuItemImageScale::create()
{
    MenuItemImageScale *pRet = new MenuItemImageScale();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

bool MenuItemImageScale::init(void)
{
    return initWithFrameName(nullptr, 1.1f, (const ccMenuCallback&)nullptr);
}

MenuItemImageScale * MenuItemImageScale::create(const char *frameName, float selectScale, const ccMenuCallback& callback)
{
    MenuItemImageScale *pRet = new MenuItemImageScale();
    if (pRet && pRet->initWithFrameName(frameName, selectScale, callback))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

bool MenuItemImageScale::initWithFrameName(const char *frameName, float selectScale, const ccMenuCallback& callback)
{
    if (MenuItemImage::initWithCallback(callback))
    {
        if (frameName && strlen(frameName) > 0)
            setNormalSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(frameName));
        else
            setNormalImage(NULL);
        
        setSelectedImage(NULL);
        setDisabledImage(NULL);
        
        _selectedScale = selectScale;
        
        return true;
    }
    return false;
}

void MenuItemImageScale::selected()
{
    MenuItem::selected();
    stopAllActions();
    runAction(EaseElasticOut::create(ScaleTo::create(0.4f, _selectedScale)));
}

void MenuItemImageScale::unselected()
{
    MenuItem::unselected();
    stopAllActions();
    runAction(EaseElasticOut::create(ScaleTo::create(0.4f, _normalScale)));
}

void MenuItemImageScale::setEnabled(bool bEnabled)
{
    if( _enabled != bEnabled )
    {
        MenuItem::setEnabled(bEnabled);
        if (bEnabled)
            _normalImage->setColor(Color3B(255, 255, 255));
        else
            _normalImage->setColor(Color3B(100, 100, 100));
    }
}

void MenuItemImageScale::updateImagesVisibility()
{
    if(_normalImage)
        _normalScale = _normalImage->getScale();
}

