//
//  MenuItemImageScale.h
//

#ifndef __MenuItemImageScale__
#define __MenuItemImageScale__

#include "cocos2d.h"

USING_NS_CC;

class MenuItemImageScale : public MenuItemImage
{    
    float _normalScale;
    float _selectedScale;
    
public:
    
    MenuItemImageScale(){}
    virtual ~MenuItemImageScale(){}
    
    static MenuItemImageScale* create();
    static MenuItemImageScale* create(const char *frameName, float selectScale, const ccMenuCallback& callback);
    
    bool init();
    bool initWithFrameName(const char *frameName, float selectScale, const ccMenuCallback& callback);
    
    void setSelectedScale(float scale) { _selectedScale = scale; };
    
    virtual void selected();
    virtual void unselected();
    virtual void setEnabled(bool bEnabled);
    
protected:
    virtual void updateImagesVisibility();
};

#endif
