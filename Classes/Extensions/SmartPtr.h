//
//  SmartPtr.h
//

#ifndef _CCPtr_h
#define _CCPtr_h

#include <memory>

template <typename T>
auto _unique(T* object)->std::unique_ptr<T, std::function<void (T*)>> {
    return std::unique_ptr<T, std::function<void (T*)>>(object, [](T* obj) { obj->release(); });
}

template <typename T>
auto _unique_auto(T* object)->std::unique_ptr<T, std::function<void (T*)>> {
    auto res = std::unique_ptr<T, std::function<void (T*)>>(object, [](T* obj) { obj->release(); });
    res->retain();
    return res;
}

template <typename T>
auto _unique_node(T* object)->std::unique_ptr<T, std::function<void (T*)>> {
    return std::unique_ptr<T, std::function<void (T*)>>(object, [](T* obj) { obj->removeFromParentAndCleanup(true); obj->release(); });
}

template <typename T>
auto _shared(T* object)->std::shared_ptr<T> {
    return std::shared_ptr<T>(object, [](T* obj) { obj->release(); });
}

template <typename T>
auto _shared_auto(T* object)->std::shared_ptr<T> {
    auto res = std::shared_ptr<T>(object, [](T* obj) { obj->release(); });
    res->retain();
    return res;
}

#endif
