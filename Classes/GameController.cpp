//
//  GameController.cpp
//

#include "GameController.h"
#include "AssetsLoader.h"
#include "LoadingLayer.h"
#include "GameLayer.h"
#include "GameUILayer.h"

using namespace c2d;

const string LOADING_SCENE_ASSETS = "LoadingScene.json";
const string GAME_SCENE_ASSETS    = "GameScene.json";

GameController::GameController() {
    
}

auto GameController::onStart()->void {
    _showLoadingScene();
}

auto GameController::onPause()->void {

}

auto GameController::onResume()->void {
    
}

auto GameController::_showLoadingScene()->void {
    auto scene_assets = new AssetsLoader(LOADING_SCENE_ASSETS);
    scene_assets->setUnloadResourcesOnDestroy(true);
    scene_assets->setOnCompleteHandler([this, scene_assets]() {
        shared_ptr<AssetsLoader> shared_sa(scene_assets);
        auto layer = LoadingLayer::create(shared_sa);
        layer->setOnShowedCallback([&, this, layer]() {
            vector<string> assets_list;
            assets_list.push_back(GAME_SCENE_ASSETS);
            auto all_assets = new AssetsLoader(assets_list, CC_CALLBACK_1(LoadingLayer::stepProgress, layer));
            all_assets->setOnCompleteHandler([&, this, all_assets, layer](){
                delete all_assets;
                layer->hideLoadingBar([&, this]() {
                    _showGameScene();
                });
            });
        });
        auto scene = Scene::create();
        scene->addChild(layer);
        Director::getInstance()->runWithScene(scene);
    });
}

auto GameController::_showGameScene()->void {
    auto scene_assets = new AssetsLoader(GAME_SCENE_ASSETS);
    scene_assets->setOnCompleteHandler([this, scene_assets](){
        shared_ptr<AssetsLoader> shared_sa(scene_assets);
        auto layer = GameLayer::create(shared_sa);
        layer->setReloadGameHandler(CC_CALLBACK_0(GameController::_showGameScene, this));
        auto ui_layer = GameUILayer::create(shared_sa);
        auto scene = Scene::create();
        scene->addChild(layer);
        scene->addChild(ui_layer);
        Director::getInstance()->replaceScene(TransitionFade::create(0.8f, scene));
    });
}

