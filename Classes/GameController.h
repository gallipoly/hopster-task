//
//  GameController.h
//

#ifndef __GameController__
#define __GameController__

#include "cocos2d.h"

USING_NS_CC;

class GameController {
public:
    GameController();
    
    auto onStart()->void;
    auto onPause()->void;
    auto onResume()->void;
    
protected:
    auto _showLoadingScene()->void;
    auto _showGameScene()->void;
};


#endif
