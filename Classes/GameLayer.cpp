//
//  GameLayer.cpp
//

#include "GameLayer.h"
#include "Constants.h"


GameLayer* GameLayer::create(shared_ptr<AssetsLoader> builder) {
    GameLayer *ret = new (std::nothrow) GameLayer(builder);
    if (ret && ret->init()) {
        ret->autorelease();
        return ret;
    }
    else {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}

GameLayer::GameLayer(shared_ptr<AssetsLoader> builder)
: BasicLayer(builder)
{

}

bool GameLayer::init() {
    if (!Layer::init()) {
        return false;
    }
    
    auto background = Sprite::create("Background.png");
    background->setPosition(_winSize.width / 2.f, _winSize.height / 2.f);
    addChild(background, -1);
    
    auto foreground = Sprite::create("Foreground.png");
    foreground->setPosition(_winSize.width / 2.f, _winSize.height / 2.f);
    addChild(foreground, _winSize.height + 1);
    
    
    auto light_clipper = _shared_auto(ClippingNode::create());
    light_clipper->setInverted(true);
    addChild(light_clipper.get(), 1);
    
    auto shadow_clipper = _shared_auto(ClippingNode::create());
    addChild(shadow_clipper.get(), 1);
    
    auto lightBorders = Rect(0.f, 0.f, _winSize.width, _winSize.height);
    _lightMapGenerator = _shared_auto(LightMapGenerator::create(lightBorders));
    
    light_clipper->setStencil(_lightMapGenerator->getShadowsNode());
    shadow_clipper->setStencil(_lightMapGenerator->getShadowsNode());
    
    
    _flame = _shared_auto(FlameObject::create(light_clipper, shadow_clipper));
    _flame->setPosition(_winSize.width / 2.f, _winSize.height / 2.f);
    _flame->setAnchorPoint(Vec2(0.5f, 0.1f));
    addChild(_flame.get());
    
    
    createLevel();

    for (auto decor : _decorations) {
        _collisionShapes.push_back(decor->getShape());
    }
    _lightMapGenerator->setShapes(_collisionShapes);
    _lightMapGenerator->updateLight(_flame->getPosition());
    
    return true;
}

void GameLayer::createLevel() {
    vector<Vec2> decorPositions = {
        Vec2(180.f, 180.f),
        Vec2(400.f, 300.f),
        Vec2(200.f, 500.f),
        Vec2(700.f, 400.f),
        Vec2(500.f, 600.f),
        Vec2(550.f, 250.f),
        Vec2(800.f, 200.f),
        Vec2(750.f, 700.f)
    };
    for (auto pos : decorPositions) {
        auto decor = _shared_auto(DecorObject::create());
        decor->setPosition(pos.x, pos.y);
        addChild(decor.get());
        _decorations.push_back(decor);
    }
    
    vector<Vec2> creaturePositions = {
        Vec2(400.f, 70.f),
        Vec2(300.f, 200.f),
        Vec2(100.f, 300.f),
        Vec2(780.f, 260.f),
        Vec2(300.f, 450.f),
        Vec2(600.f, 100.f),
        Vec2(900.f, 400.f),
        Vec2(500.f, 670.f),
        Vec2(300.f, 650.f)
    };
    for (auto pos : creaturePositions) {
        auto creature = _shared_auto(CreatureObject::create());
        creature->setPosition(pos.x, pos.y);
        creature->setAnchorPoint(Vec2(0.5f, 0.1f));
        addChild(creature.get());
        _creatures.push_back(creature);
    }
}

void GameLayer::onEnter() {
    Layer::onEnter();
    
    if (_touchEventListener == NULL) {
        _touchEventListener = EventListenerTouchOneByOne::create();
        _touchEventListener->onTouchBegan = CC_CALLBACK_2(GameLayer::onTouchBegan, this);
        _touchEventListener->onTouchMoved = CC_CALLBACK_2(GameLayer::onTouchMoved, this);
        _touchEventListener->onTouchEnded = CC_CALLBACK_2(GameLayer::onTouchEnded, this);
        _touchEventListener->onTouchCancelled = CC_CALLBACK_2(GameLayer::onTouchCancelled, this);
    }
    if (_keyEventListener == NULL) {
        _keyEventListener = EventListenerKeyboard::create();
        _keyEventListener->onKeyReleased = CC_CALLBACK_2(GameLayer::onKeyReleased, this);
    }
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority( _touchEventListener, this );
    _eventDispatcher->addEventListenerWithSceneGraphPriority( _keyEventListener, this );
    
    _fireButtonEventListener = _eventDispatcher->addCustomEventListener(FIRE_BUTTON_PRESSED_EVENT, CC_CALLBACK_1(GameLayer::onFireButtonPressed, this));
    _reloadButtonEventListener = _eventDispatcher->addCustomEventListener(RELOAD_BUTTON_PRESSED_EVENT, CC_CALLBACK_1(GameLayer::onReloadButtonPressed, this));
    
    scheduleUpdate();
}

void GameLayer::onExit() {
    Layer::onExit();
    
    _eventDispatcher->removeEventListener( _touchEventListener );
    _eventDispatcher->removeEventListener( _keyEventListener );
    _eventDispatcher->removeEventListener( _fireButtonEventListener );
    _eventDispatcher->removeEventListener( _reloadButtonEventListener );
    
    unscheduleUpdate();
}

void GameLayer::update(float dt) {
    if (_lightMapGenerator->isVisible()) {
        _lightMapGenerator->updateLight(_flame->getPosition());
    }
}

bool GameLayer::onTouchBegan(Touch* touch, Event* event) {
    auto pt = this->convertTouchToNodeSpace(touch);
    
    auto flame_position = _flame->getPosition();
    auto select_radius = 80.f;
    if (flame_position.distance(pt) > select_radius) {
        return false;
    }

    _flame->setLocalZOrder(_winSize.height);
    _previousTouchPosition = pt;
    
    return true;
}

void GameLayer::onTouchMoved(Touch* touch, Event* event) {
    auto pt = this->convertTouchToNodeSpace(touch);

    auto pos = _flame->getPosition() + (pt - _previousTouchPosition);

    _flame->setPosition(pos.x, pos.y);
    _flame->setLocalZOrder(_winSize.height);
    _previousTouchPosition = pt;
    
    checkLightVisibility();
}

void GameLayer::onTouchEnded(Touch* touch, Event* event) {
    auto pt = this->convertTouchToNodeSpace(touch);
    
    _flame->setLocalZOrder(_winSize.height - _flame->getPosition().y);
}

void GameLayer::onTouchCancelled(Touch* touch, Event* event) {
    onTouchEnded(touch, event);
}

void GameLayer::checkLightVisibility() {
    auto visible = !geometry::isCircleInsideAnyShape(_collisionShapes, _flame->getCollisioneCircle());
    _flame->enableLight(visible);
}

void GameLayer::checkWinCondition() {
    if (!_creatures.size()) {
        auto label = Label::createWithTTF("You Win!", FONT_NEURON, 120.f);
        label->setTextColor(Color4B(250, 225, 33, 255));
        label->enableOutline(Color4B(0, 60, 0, 255), 4);
        label->enableShadow(Color4B(100, 10, 140, 170), Size(-1, -1), 8.f);
        label->setPosition(_winSize.width * 0.5f, _winSize.height * 0.5f);
        addChild(label, _winSize.height);
        
        label->setScale(.4f);
        label->setVisible(false);
        label->runAction(Sequence::create(DelayTime::create(2.f),
                                          Show::create(),
                                          ScaleTo::create(.2, 1.15f),
                                          ScaleTo::create(0.2, 1.f),
                                          DelayTime::create(2.f),
                                          CallFunc::create(_reloadGameHandler),
                                          nullptr));
    }
}

void GameLayer::onFireButtonPressed(EventCustom* event) {
    _flame->showExplosion();
    
    list<shared_ptr<CreatureObject>>  lighted_creatures;
    for (auto creature : _creatures) {
        auto circle = creature->getCollisioneCircle();
        if (!_lightMapGenerator->isCircleInsideAnyShadow(circle)) {
            lighted_creatures.push_back(creature);
            creature->setEnabled(false);
            creature->runAction(Sequence::create(DelayTime::create(0.3f),
                                                 FadeOut::create(0.6f),
                                                 Hide::create(),
                                                 nullptr));
        }
    }
    
    for (auto creature : lighted_creatures) {
        _creatures.remove(creature);
    }
    lighted_creatures.clear();
    
    checkWinCondition();
}

void GameLayer::onReloadButtonPressed(EventCustom* event) {
    if (_reloadGameHandler) {
        _reloadGameHandler();
    }
}


