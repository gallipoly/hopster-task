//
//  GameLayer.h
//

#ifndef __GameLayer__
#define __GameLayer__

#include "cocos2d.h"
#include "BasicLayer.h"
#include "LightMapGenerator.h"
#include "FlameObject.h"
#include "DecorObject.h"
#include "CreatureObject.h"

USING_NS_CC;

class GameLayer : public BasicLayer
{
public:
    static GameLayer* create(shared_ptr<AssetsLoader> builder);
    GameLayer(shared_ptr<AssetsLoader> builder);

    virtual auto init()->bool override;
    virtual auto update( float dt )->void override;

    virtual auto onEnter()->void override;
    virtual auto onExit()->void override;

    virtual auto onTouchBegan(Touch* touch, Event* event)->bool override;
    virtual auto onTouchMoved(Touch* touch, Event* event)->void override;
    virtual auto onTouchEnded(Touch* touch, Event* event)->void override;
    virtual auto onTouchCancelled(Touch* touch, Event* event)->void override;

public:
    auto setReloadGameHandler(const function<void(void)>& handler)->void { _reloadGameHandler = handler; }
    
    auto onFireButtonPressed(EventCustom* event)->void;
    auto onReloadButtonPressed(EventCustom* event)->void;

private:
    auto createLevel()->void;
    
    auto checkLightVisibility()->void;
    auto checkWinCondition()->void;
    
private:
    EventListenerKeyboard*      _keyEventListener = NULL;
    EventListenerTouchOneByOne* _touchEventListener = NULL;
    EventListenerCustom*        _fireButtonEventListener = NULL;
    EventListenerCustom*        _reloadButtonEventListener = NULL;
    
    function<void(void)>        _reloadGameHandler;
    
private:
    shared_ptr<LightMapGenerator>       _lightMapGenerator;
    
    shared_ptr<FlameObject>             _flame;
    list<shared_ptr<DecorObject>>       _decorations;
    list<shared_ptr<CreatureObject>>    _creatures;
    
    vector<Polygon>                     _collisionShapes;
    
    Point                               _previousTouchPosition;
};

#endif
