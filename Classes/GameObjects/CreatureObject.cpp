//
//  CreatureObject.cpp
//

#include "CreatureObject.h"

const vector<string> FRAME_NAMES = {"leefy_happy.png", "petal_happy.png", "shark_happy.png"};

CreatureObject* CreatureObject::create() {
    CreatureObject *ret = new (std::nothrow) CreatureObject();
    if (ret && ret->init()) {
        ret->autorelease();
        return ret;
    }
    else {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}

CreatureObject::CreatureObject() {
    _type = "creture";
}

auto CreatureObject::init()->bool {
    auto frame = FRAME_NAMES[rand() % FRAME_NAMES.size()]; // frame name must be in level config
    if (!GameObject::init(frame)) {
        return false;
    }

    return true;
}
