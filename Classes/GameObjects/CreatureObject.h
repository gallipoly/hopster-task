//
//  CreatureObject.h
//
//

#ifndef CreatureObject_h
#define CreatureObject_h


#include "GameObject.h"

USING_NS_CC;
using namespace c2d;


class CreatureObject : public GameObject {
public:
    static CreatureObject* create();
    CreatureObject();
    
    virtual auto init()->bool override;
    
    virtual auto setEnabled(bool enabled)->void { _enabled = enabled; }
    virtual auto isEnabled()->bool { return _enabled; }
    
protected:
    bool _enabled;
};

#endif
