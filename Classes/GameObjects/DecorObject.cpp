//
//  DecorObject.cpp
//

#include "DecorObject.h"


DecorObject* DecorObject::create() {
    DecorObject *ret = new (std::nothrow) DecorObject();
    if (ret && ret->init()) {
        ret->autorelease();
        return ret;
    }
    else {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}

DecorObject::DecorObject() {
    _type = "decor";
}

auto DecorObject::init()->bool {
    if (!GameObject::init("spikeball.png")) { // frame name & shape must be in level config
        return false;
    }

    _shape.push_back(Vec2(-35, 0));
    _shape.push_back(Vec2(-20, -20));
    _shape.push_back(Vec2(0, -35));
    _shape.push_back(Vec2(20, -20));
    _shape.push_back(Vec2(35, 0));
    _shape.push_back(Vec2(20, 20));
    _shape.push_back(Vec2(0, 35));
    _shape.push_back(Vec2(20, 20));
    
    return true;
}

Polygon DecorObject::getShape() {
    auto pos = getPosition();
    Polygon worldShape;
    for (auto point : _shape) {
        worldShape.push_back(pos + point);
    }
    return worldShape;
}
