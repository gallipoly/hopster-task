//
//  DecorObject.h
//

#ifndef DecorObject_h
#define DecorObject_h


#include "GameObject.h"

USING_NS_CC;
using namespace c2d;


class DecorObject : public GameObject {
public:
    static DecorObject* create();
    DecorObject();
    
    virtual auto init()->bool override;
    
    auto getShape()->Polygon;
    
protected:
    Polygon     _shape;
};


#endif
