//
//  FlameObject.cpp
//

#include "FlameObject.h"


FlameObject* FlameObject::create(shared_ptr<ClippingNode> light, shared_ptr<ClippingNode> shadow) {
    FlameObject *ret = new (std::nothrow) FlameObject(light, shadow);
    if (ret && ret->init()) {
        ret->autorelease();
        return ret;
    }
    else {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}

FlameObject::FlameObject(shared_ptr<ClippingNode> light, shared_ptr<ClippingNode> shadow)
: _lightClipper(light)
, _shadowClipper(shadow)
{
    _type = "flame";
}

auto FlameObject::init()->bool {
    if (!GameObject::init("flame1.png")) {  // frame name must be in level config
        return false;
    }
    
    auto animation = AnimationCache::getInstance()->getAnimation("flameIdle");
    runAction(RepeatForever::create(Animate::create(animation)));
        
    _shadow = _shared_auto(LayerColor::create(Color4B(0, 0, 0, 0)));
    _shadow->setPosition(Point::ZERO);
    _lightClipper->addChild(_shadow.get());
    
    _staticShadow = _shared_auto(LayerColor::create(Color4B(0, 0, 0, 95)));
    _staticShadow->setPosition(Point::ZERO);
    _shadowClipper->addChild(_staticShadow.get());
    _staticShadow->runAction(RepeatForever::create(Sequence::createWithTwoActions(EaseInOut::create(FadeTo::create(1.4f, 105), 1.8f),
                                                                                  EaseInOut::create(FadeTo::create(1.4f, 85), 1.8f))));
    
    _light = _shared_auto(Sprite::createWithSpriteFrameName("light.png"));
    _light->setOpacity(210);
    _light->setCascadeOpacityEnabled(true);
    _light->setPosition(getPosition());
    _lightClipper->addChild(_light.get());
    
    auto shine1 = _shared_auto(Sprite::createWithSpriteFrameName("shine.png"));
    shine1->setPosition(_light->getContentSize().width / 2.f, _light->getContentSize().height / 2.f);
    _light->addChild(shine1.get());
    auto shine2 = _shared_auto(Sprite::createWithSpriteFrameName("shine.png"));
    shine2->setPosition(shine1->getPosition());
    _light->addChild(shine2.get(), 3);
    
    shine1->runAction(RepeatForever::create(RotateBy::create(11.f, -360.f)));
    shine1->runAction(RepeatForever::create(Sequence::create(EaseInOut::create(ScaleTo::create(2.5f, 2.4f), 2.f),
                                                             EaseInOut::create(ScaleTo::create(1.5f, 1.6f), 2.f), NULL)));
    shine2->runAction(RepeatForever::create(RotateBy::create(11.f, 360.f)));
    shine2->runAction(RepeatForever::create(Sequence::create(EaseInOut::create(ScaleTo::create(2.5f, 2.3f), 2.f),
                                                             EaseInOut::create(ScaleTo::create(1.5f, 1.5f), 2.f), NULL)));
    
    auto light = _light;
    auto func = [light](void) {
        auto light_circle = Sprite::createWithSpriteFrameName("light_circle.png");
        light_circle->setPosition(light->getContentSize().width / 2.f, light->getContentSize().height / 2.f);
        light_circle->setScale(0.f);
        light->addChild(light_circle, 3.f);
        light_circle->runAction(Sequence::create(FadeIn::create(0.5f), DelayTime::create(1.5f), FadeOut::create(1.0f), NULL));
        light_circle->runAction(Sequence::create(ScaleTo::create(3.0f, 4.f), RemoveSelf::create(), NULL));
    };
    runAction(RepeatForever::create(Sequence::create(CallFunc::create(func), DelayTime::create(1.0f), NULL)));
    
    return true;
}

auto FlameObject::setPosition(float x, float y)->void {
    GameObject::setPosition(x, y);
    if (_light) {
        _light->setPosition(Vec2(x, y));
    }
}

auto FlameObject::enableLight(bool enable)->void {
    _light->setVisible(enable);
    _shadow->setVisible(enable);
    _staticShadow->setVisible(enable);
}

auto FlameObject::showExplosion()->void {
    auto light_circle = Sprite::createWithSpriteFrameName("fire_blow.png");
    light_circle->setPosition(_light->getContentSize().width / 2.f, _light->getContentSize().height / 2.f);
    _light->addChild(light_circle, 2.f);
    
    light_circle->setScale(0.f);
    light_circle->runAction(Sequence::create(FadeIn::create(0.2f), DelayTime::create(0.6f), FadeOut::create(0.4f), NULL));
    light_circle->runAction(Sequence::create(ScaleTo::create(1.2f, 4.f), RemoveSelf::create(), NULL));
    light_circle->runAction(RotateBy::create(1.2f, 180.f));
}
