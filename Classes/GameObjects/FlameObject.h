//
//  FlameObject.hpp
//

#ifndef FlameObject_h
#define FlameObject_h


#include "GameObject.h"

USING_NS_CC;
using namespace c2d;


class FlameObject : public GameObject {
public:
    static FlameObject* create(shared_ptr<ClippingNode> light, shared_ptr<ClippingNode> shadow);
    FlameObject(shared_ptr<ClippingNode> light, shared_ptr<ClippingNode> shadow);
    
    virtual auto init()->bool override;
    virtual auto setPosition(float x, float y)->void override;
    
    auto enableLight(bool enable)->void;
    auto showExplosion()->void;
    
protected:
    shared_ptr<ClippingNode>        _lightClipper;
    shared_ptr<ClippingNode>        _shadowClipper;
    
    shared_ptr<LayerColor>          _shadow;
    shared_ptr<LayerColor>          _staticShadow;
    shared_ptr<Sprite>              _light;
};


#endif
