//
//  GameObject.cpp
//

#include "GameObject.h"

const float GO_BASE_COLLISION_RADIUS = 20.f;

GameObject* GameObject::create(const string& frameName) {
    GameObject *ret = new (std::nothrow) GameObject();
    if (ret && ret->init(frameName)) {
        ret->autorelease();
        return ret;
    }
    else {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}

GameObject::GameObject() {
    _type = "base";
    _collisionRadius = GO_BASE_COLLISION_RADIUS;
}

auto GameObject::init(const string& frameName)->bool {
    if (!Sprite::initWithSpriteFrameName(frameName)) {
        return false;
    }
    
    return true;
}

auto GameObject::update(float dt)->void {
    
}

auto GameObject::setPosition(float x, float y)->void {
    Sprite::setPosition(x, y);
    auto h = Director::getInstance()->getWinSize().height;
    setLocalZOrder(h - y);
}
