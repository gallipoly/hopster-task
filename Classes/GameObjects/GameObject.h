//
//  GameObject.hpp
//

#ifndef GameObject_h
#define GameObject_h

#include "cocos2d.h"

USING_NS_CC;
using namespace c2d::geometry;


class GameObject : public Sprite {
public:
    static GameObject* create(const string& frameName);
    GameObject();
    
    virtual auto init(const string& frameName)->bool;
    
    virtual auto update(float dt)->void override;
    virtual auto setPosition(float x, float y)->void override;
    
    virtual auto getType()->const string { return _type; }
    virtual auto getCollisioneCircle()->Circle { return Circle(getPosition(), _collisionRadius); }
    
protected:
    string  _type;
    float   _collisionRadius;
};

#endif
