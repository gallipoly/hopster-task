//
//  GameUILayer.cpp
//

#include "GameUILayer.h"
#include "MenuItemImageScale.h"
#include "Constants.h"


GameUILayer* GameUILayer::create(shared_ptr<AssetsLoader> builder) {
    GameUILayer *ret = new (std::nothrow) GameUILayer(builder);
    if (ret && ret->init()) {
        ret->autorelease();
        return ret;
    }
    else {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}

GameUILayer::GameUILayer(shared_ptr<AssetsLoader> builder)
: BasicLayer(builder)
{
    
}

bool GameUILayer::init() {
    if (!BasicLayer::init()) {
        return false;
    }
    
    auto fire_btn = MenuItemImageScale::create("button_green.png", 1.1f, [this](Ref* ref) {
        auto event = _shared(new EventCustom(FIRE_BUTTON_PRESSED_EVENT));
        _eventDispatcher->dispatchEvent(event.get());
    });
    fire_btn->setPosition(100.f, 50.f);
    
    auto fire_label = Label::createWithTTF("Fire", FONT_NEURON, 40.f);
    fire_label->setTextColor(Color4B(250, 225, 33, 255));
    fire_label->enableOutline(Color4B(0, 60, 0, 255), 1);
    fire_label->enableShadow(Color4B(100, 10, 140, 170), Size(-1, -1), 2.f);
    fire_label->setPosition(fire_btn->getContentSize().width * 0.5f, fire_btn->getContentSize().height * 0.6f);
    fire_btn->addChild(fire_label);
    
    auto reload_btn = MenuItemImageScale::create("button_green.png", 1.1f, [this](Ref* ref) {
        auto event = _shared(new EventCustom(RELOAD_BUTTON_PRESSED_EVENT));
        _eventDispatcher->dispatchEvent(event.get());
    });
    reload_btn->setPosition(_winSize.width - 100.f, 50.f);
    
    auto reload_label = Label::createWithTTF("Reload", FONT_NEURON, 40.f);
    reload_label->setTextColor(Color4B(250, 225, 33, 255));
    reload_label->enableOutline(Color4B(0, 60, 0, 255), 1);
    reload_label->enableShadow(Color4B(100, 10, 140, 170), Size(-1, -1), 2.f);
    reload_label->setPosition(fire_btn->getContentSize().width * 0.5f, fire_btn->getContentSize().height * 0.6f);
    reload_btn->addChild(reload_label);
    
    auto menu = Menu::create(fire_btn, reload_btn, NULL);
    menu->setPosition(0.f, 0.f);
    addChild(menu);
    
    return true;
}
