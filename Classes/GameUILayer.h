//
//  GameUILayer.hpp
//

#ifndef GameUILayer_h
#define GameUILayer_h

#include "cocos2d.h"
#include "BasicLayer.h"

USING_NS_CC;


class GameUILayer : public BasicLayer
{
public:
    static GameUILayer* create(shared_ptr<AssetsLoader> builder);
    GameUILayer(shared_ptr<AssetsLoader> builder);
    
    virtual auto init()->bool override;
};

#endif
