//
//  SplashScene.cpp
//

#include "LoadingLayer.h"
#include "Constants.h"

const float TIME_SCALE = 1.0f;

LoadingLayer* LoadingLayer::create(shared_ptr<AssetsLoader> builder) {
    LoadingLayer *ret = new (std::nothrow) LoadingLayer(builder);
    if (ret && ret->init()) {
        ret->autorelease();
        return ret;
    }
    else {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}

LoadingLayer::LoadingLayer(shared_ptr<AssetsLoader> builder)
: BasicLayer(builder)
{

}

bool LoadingLayer::init() {
    if (Layer::init()) {
        Size s = Director::getInstance()->getWinSize();
        
        _preloader = Sprite::createWithSpriteFrameName("progress_bar_back.png");
        _preloader->setPosition(s.width / 2.f, s.height / 2.f);
        addChild(_preloader);
        
        _preloader->setScaleX(0.f);
        _preloader->runAction(EaseBackOut::create(ScaleTo::create(0.7f * TIME_SCALE, 1.f, 1.f)));
        
        auto preloader_center = Sprite::createWithSpriteFrameName("progress_bar_center.png");
        preloader_center->setPosition(_preloader->getContentSize().width / 2.f, _preloader->getContentSize().height / 2.f);
        _preloader->addChild(preloader_center);
        
        preloader_center->setFlippedX(true);
        preloader_center->runAction(RepeatForever::create((RotateBy::create(1.f, 360.f))));
        
        auto preloader_front = Sprite::createWithSpriteFrameName("progress_bar_front.png");
        preloader_front->setPosition(_preloader->getContentSize().width / 2.f, _preloader->getContentSize().height / 2.f);
        _preloader->addChild(preloader_front);
        
        _progressLabel = Label::createWithTTF("Loading 0%", FONT_NEURON, 20.f);
        if (!_progressLabel)
            _progressLabel = Label::createWithSystemFont("Loading 0%", "Arial", 20.f);
        _progressLabel->setPosition(_preloader->getPosition());
        addChild(_progressLabel, -1);
        
        _progressLabel->setScaleX(0.f);
        _progressLabel->runAction(EaseBackOut::create(MoveTo::create(0.5f * TIME_SCALE, Point(_preloader->getPosition().x, _preloader->getPosition().y - _preloader->getContentSize().height * 0.65f))));
        _progressLabel->runAction(EaseBackOut::create(ScaleTo::create(0.5f * TIME_SCALE, 1.f, 1.f)));
        
        return true;
    }
    return false;
}

void LoadingLayer::stepProgress(float progress) {
    char str[64];
    sprintf(str, "Loading %.0f%%", progress);
    _progressLabel->setString(str);
}

void LoadingLayer::setOnShowedCallback(const function<void(void)>& callback) {
    _onShowed = callback;
    runAction(Sequence::createWithTwoActions(DelayTime::create(0.5f * TIME_SCALE), CallFunc::create(_onShowed)));
}

auto LoadingLayer::hideLoadingBar(const function<void(void)>& callback)->void {
    _progressLabel->runAction(EaseBackIn::create(ScaleTo::create(0.5f * TIME_SCALE, 0.f, 1.f)));
    _progressLabel->runAction(EaseBackIn::create(MoveTo::create(0.5f * TIME_SCALE, _preloader->getPosition())));
    
    _preloader->runAction(Sequence::createWithTwoActions(EaseBackIn::create(ScaleTo::create(0.7f * TIME_SCALE, 0.f, 0.f)),
                                                         CallFunc::create(callback)));
}

