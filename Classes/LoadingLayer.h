//
//  LoadingLayer.h
//

#ifndef __LoadingLayer__
#define __LoadingLayer__

#include "cocos2d.h"
#include "BasicLayer.h"

USING_NS_CC;

class LoadingLayer : public BasicLayer
{
public:
    static LoadingLayer* create(shared_ptr<AssetsLoader> builder);
    LoadingLayer(shared_ptr<AssetsLoader> builder);
    
    virtual auto init()->bool override;
    
    auto stepProgress(float progress)->void;
    
    auto setOnShowedCallback(const function<void(void)>& callback)->void;
    auto hideLoadingBar(const function<void(void)>& callback)->void;
    
protected:
    function<void(void)>        _onShowed;
    
    Sprite*                     _preloader;
    Label*                      _progressLabel;
};


#endif /* defined(__SplashScene__) */
