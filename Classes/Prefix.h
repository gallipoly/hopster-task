
#ifdef __cplusplus
    #include <string>
    #include <set>
    #include <vector>
    #include <map>
    #include <memory>
    #include <functional>
    #include <algorithm>
    #include <sstream>
    #include <iostream>
    #include <stdio.h>
    #include <stdlib.h>
    #include <istream>
    #include <assert.h>
    #include <mutex>

    #include "Extensions/SmartPtr.h"
    #include "Extensions/Geometry.h"

    using namespace std;
#endif
